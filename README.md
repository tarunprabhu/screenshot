# screenshot

`screenshot` is a convenience wrapper around [`maim`](https://github.com/naelstrof/maim)
and [`fbgrab`](https://github.com/GunnarMonell/fbgrab) to take screenshots on 
both virtual consoles and GUI sessions. 

# Requirements

`screenshot` requires several other utilities to be installed on the system. 
These are best obtained from the package manager of your distribution. 

- [`xclip`](https://github.com/astrand/xclip)
- [`xdotool`](https://github.com/jordansissel/xdotool)
- [`xrandr`](https://gitlab.freedesktop.org/xorg/app/xrandr)

`screenshot` is written in Lua and requires a Lua interpreter to be installed
in addition to some packages.

## Interpreter

Lua >= 5.2

## Packages

These can be installed using [LuaRocks](https://luarocks.org). They may 
also be present in the system's repositories.

- [argparse](https://github.com/mpeterv/argparse)
- [luaposix](https://github.com/luaposix/luaposix)

# Installation

The `screenshot` script can be downloaded separately and copied to a directory 
that is in $PATH. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
screenshot -h
```
